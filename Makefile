NM_NAME = ft_nm
OTOOL_NAME = ft_otool

NM_SRCS_DIR = nm/
OTOOL_SRCS_DIR = otool/
COMMON_SRCS_DIR = common/

NM_SRCS_FILES = nm.c symtab32.c print_infos.c symtab64.c\
	get_sect.c parse_type.c place_node_lst.c\
	remove_duplicates.c get_name.c handle_errors.c reverse.c\
	map_files.c parse_mnumber.c handle_ranlib.c lst_rl.c\
	handle_fatbin.c lst_fs.c
	
OTOOL_SRCS_FILES = otool.c symtab32.c display.c symtab64.c\
	handle_errors.c reverse.c map_files.c parse_mnumber.c\
	handle_ranlib.c lst_rl.c handle_fatbin.c lst_fs.c

NM_SRCS = $(addprefix $(NM_SRCS_DIR), $(NM_SRCS_FILES))
OTOOL_SRCS = $(addprefix $(OTOOL_SRCS_DIR), $(OTOOL_SRCS_FILES))

NM_OBJS = $(NM_SRCS:.c=.o)
OTOOL_OBJS = $(OTOOL_SRCS:.c=.o)

all: build $(NM_NAME) $(OTOOL_NAME)

build:
	@make -C libft/

%.o: %.c
	@gcc -g3 -o $@ -c $<

$(NM_NAME): $(NM_OBJS)
	@gcc -Wall -Wextra -Werror -o $@ $^ -L libft/ -lft
	@echo "\033[1;35m --> ft_nm Done ! <--\033[m"

$(OTOOL_NAME): $(OTOOL_OBJS)
	@gcc -Wall -Wextra -Werror -o $@ $^ -L libft/ -lft -fsanitize=address
	@echo "\033[1;34m --> ft_otool Done ! <--\033[m"

clean:
		@rm -f $(NM_OBJS) $(OTOOL_OBJS)
		@echo "\033[1;33m --> Cleaning Done ! <--\033[m"

fclean: clean
		@rm -f $(NM_NAME) $(OTOOL_NAME)
		@cd libft && make fclean
		@echo "\033[1;31m --> Fcleaning Done ! <--\033[m"

re: fclean all

.PHONY: all clean fclean re build