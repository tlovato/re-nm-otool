/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:45:34 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:55:52 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef NM_H
# define NM_H

# include "../libft/libft.h"
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/ranlib.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <unistd.h>
# include <errno.h>

# define P_R 		PROT_READ | PROT_WRITE
# define M_P 		MAP_PRIVATE
# define LC			struct load_command
# define SC			struct symtab_command
# define RL			struct ranlib
# define FH			struct fat_header
# define FA			struct fat_arch
# define MH32		struct mach_header
# define NL32		struct nlist
# define SGC32		struct segment_command
# define SEC32		struct section
# define MH64		struct mach_header_64
# define NL64		struct nlist_64
# define SGC64		struct segment_command_64
# define SEC64		struct section_64

typedef struct		s_symb
{
	char			*name;
	unsigned long	value;
	unsigned char	type;
	struct s_symb	*next;
}					t_symb;

typedef struct		s_var
{
	int				max;
	char			*table;
	char			*arsect;
	uint32_t		table_len;
}					t_var;

typedef struct		s_rl
{
	int				offset;
	int				offname;
	int				lenname;
	struct s_rl		*next;
}					t_rl;

typedef struct		s_fs
{
	int				mnumber;
	unsigned long	offset;
	unsigned long	size;
	cpu_type_t		cput;
	struct s_fs		*next;
}					t_fs;

void				clean_lst(t_fs **lst);
void				get_sect32(LC *lc, char *arsect, size_t order);
void				get_sect64(LC *lc, char *arsect, size_t order);
void				place_node_fs(t_fs **lst, t_fs *new);
void				place_node_ascii(t_symb **lst, t_symb *new);
void				print_infos(int nbits, t_symb *lst);
void				remove_duplicates_ascii(t_symb **lst);
char				*rev(char *buf, size_t size);
char				rev_byte(char val);
int					symtab32(char *buf, size_t size, int order);
int					symtab64(char *buf, size_t size, int order);
char				type(unsigned char t, unsigned char s, char *ar, int val);
char				rev_type(char value, char sect, char *ar, int val);
void				init_t_symb();
char				*get_name(char *buf);

int					print_errors(char *ft, char *file, int code);
char				*rev(char *buf, size_t size);
char				rev_byte(char val);
int					map_file(char *file, char *ft);
int					mnumber(char *file, char *buf, int size, char *ft);
int					handle_ranlib(char *f, char *buf, size_t order, char *ft);
void				place_node_offset(t_rl **lst, t_rl **new);
void				remove_duplicates_offset(t_rl **lst);
int					handle_fatbin(char *f, char *buf, size_t size, int order);
void				place_node_fs(t_fs **lst, t_fs *new);

#endif
