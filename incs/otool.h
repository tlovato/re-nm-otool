/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:45:14 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:55:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef OTOOL_H
# define OTOOL_H

# include "../libft/libft.h"
# include <mach-o/loader.h>
# include <mach-o/nlist.h>
# include <mach-o/fat.h>
# include <mach-o/ranlib.h>
# include <sys/stat.h>
# include <sys/mman.h>
# include <unistd.h>
# include <errno.h>

# define P_R 		PROT_READ | PROT_WRITE
# define M_P 		MAP_PRIVATE
# define LC			struct load_command
# define SC			struct symtab_command
# define RL			struct ranlib
# define FH			struct fat_header
# define FA			struct fat_arch
# define MH32		struct mach_header
# define NL32		struct nlist
# define SGC32		struct segment_command
# define SEC32		struct section
# define MH64		struct mach_header_64
# define NL64		struct nlist_64
# define SGC64		struct segment_command_64
# define SEC64		struct section_64

typedef struct		s_rl
{
	int				offset;
	int				offname;
	int				lenname;
	struct s_rl		*next;
}					t_rl;

typedef struct		s_fs
{
	int				mnumber;
	unsigned long	offset;
	unsigned long	size;
	cpu_type_t		cput;
	struct s_fs		*next;
}					t_fs;

void				display_result(int *norme, uint64_t a, char *buf, int n);
int					symtab32(char *buf, size_t size, int order);
int					symtab64(char *buf, size_t size, int order);
int					print_errors(char *ft, char *file, int code);
char				*rev(char *buf, size_t size);
char				rev_byte(char val);
int					map_file(char *file, char *ft);
int					mnumber(char *file, char *buf, int size, char *ft);
int					handle_ranlib(char *f, char *buf, size_t order, char *ft);
void				place_node_offset(t_rl **lst, t_rl **new);
void				remove_duplicates_offset(t_rl **lst);
int					handle_fatbin(char *f, char *buf, size_t size, int order);
void				place_node_fs(t_fs **lst, t_fs *new);

#endif
