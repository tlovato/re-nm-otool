/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   attrib_case.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: Pyxis <Pyxis@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/18 15:28:45 by tlovato           #+#    #+#             */
/*   Updated: 2018/02/23 22:13:42 by Pyxis            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		pars_ret(t_result *ret, int i, va_list ap)
{
	if (ret->arg && verif_convtype(ret->arg))
		convtype_case(ret, ap);
	else if (ret->arg && verif_invalid_arg(ret->arg))
		invalid_arg(ret, i);
}

static void		take_arg(t_result *ret, int i)
{
	int		j;
	int		k;

	j = i;
	k = 0;
	while (ret->cpy[j] && (is_flag(ret->cpy[j]) || is_modif(ret->cpy[j])
				|| is_precision(ret->cpy[j]) || is_width(ret->cpy[j])))
		j++;
	j += 1;
	if (is_convtype(ret->cpy[j - 1]) || ret->cpy[j - 1] == '%'
		|| ft_isalpha(ret->cpy[j - 1]))
		ret->arg = ft_strsub(ret->cpy, i, j - i);
	else
		ret->arg = NULL;
}

static void		take_sub(t_result *ret, int i)
{
	char		*j;
	int			len;

	i = ft_strlen(ret->arg) + i;
	j = ft_strchr(&ret->cpy[i], '%');
	len = j ? j - &ret->cpy[i] : ft_strlen(ret->cpy) - i;
	ret->sub = ft_strnew(len);
	ft_memcpy(ret->sub, &ret->cpy[i], len);
	ret->sub_len = len;
}

static int		parsing(t_result *ret, int i, va_list ap)
{
	ret->conv = NULL;
	ret->arg = NULL;
	ret->percent += 1;
	if (ret->res == NULL)
	{
		ret->res = ft_strsub(ret->cpy, 0, i);
		ret->res_len = i;
	}
	i++;
	take_arg(ret, i);
	if (ret->arg != NULL)
	{
		take_sub(ret, i);
		pars_ret(ret, i, ap);
		i = i + ft_strlen(ret->arg);
	}
	else
		ret->percent = 0;
	ft_strdel(&ret->conv);
	ft_strdel(&ret->arg);
	return (i);
}

void			attrib_case(t_result *ret, va_list ap, char *cpy)
{
	int			i;

	i = 0;
	while (cpy[i])
	{
		if (cpy[i] == '%')
			i = parsing(ret, i, ap);
		else
			i++;
	}
	if (ret->percent == 0)
	{
		ret->res = ft_strdup(ret->cpy);
		ret->res_len = ft_strlen(ret->res);
	}
}
