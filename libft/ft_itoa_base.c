/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/30 19:12:08 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:13:05 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int				ft_intlen(int n, int base)
{
	int					len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / base;
		len++;
	}
	return (len);
}

static char				*ft_core(int n, char *str, int base)
{
	int					len;
	char				*tab;

	tab = "0123456789ABCDEF";
	len = ft_intlen(n, base);
	str[len] = '\0';
	while (n != 0)
	{
		str[--len] = tab[n % base];
		n = n / base;
	}
	return (str);
}

char					*ft_itoa_base(int nb, int base)
{
	char				*str;
	int					i;
	int					n;

	str = (char *)malloc(ft_intlen(nb, base) + 1 * (sizeof(char)));
	if (str == NULL)
		return (NULL);
	if (base < 2 || base > 16)
		return (NULL);
	i = 0;
	n = nb;
	if (n == 0)
	{
		str[0] = '0';
		str[1] = '\0';
		return (str);
	}
	return (ft_core(n, str, base));
}
