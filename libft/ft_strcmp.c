/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 09:56:09 by tlovato           #+#    #+#             */
/*   Updated: 2016/11/07 15:19:35 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(const char *str, const char *str2)
{
	int		i;

	i = 0;
	if (!str || !str2)
		return (-1);
	while ((str[i] && str2[i]) && (str[i] == str2[i]))
		i++;
	return ((unsigned char)str[i] - (unsigned char)str2[i]);
}
