/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 17:52:16 by tlovato           #+#    #+#             */
/*   Updated: 2017/01/23 17:52:17 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strndup(char const *str, size_t len)
{
	char	*ret;
	size_t	c;

	c = 0;
	if (!(ret = malloc(sizeof(char) * len)))
		return (NULL);
	while (str[c] && c < len)
	{
		ret[c] = str[c];
		c++;
	}
	ret[c] = 0;
	return (ret);
}
