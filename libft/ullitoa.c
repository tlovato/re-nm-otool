/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ullitoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 23:20:06 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 23:20:09 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static unsigned long long		ft_intlen(unsigned long long n)
{
	unsigned long long			len;

	len = 0;
	if (n == 0)
		return (1);
	while (n > 0)
	{
		n = n / 10;
		len++;
	}
	return (len);
}

static char						*ft_core(unsigned long long n, char *str)
{
	unsigned long long			len;

	len = ft_intlen(n);
	str[len] = '\0';
	while (n != 0)
	{
		str[--len] = n % 10 + 48;
		n = n / 10;
	}
	return (str);
}

char							*ft_ullitoa(unsigned long long nb)
{
	char						*str;
	unsigned long long			i;
	unsigned long long			n;

	if ((str = (char *)malloc(ft_intlen(nb) + 1 * (sizeof(char)))) == NULL)
		return (NULL);
	i = 0;
	n = nb;
	if (n == 0)
	{
		str[0] = '0';
		str[1] = '\0';
		return (str);
	}
	return (ft_core(n, str));
}
