/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_sect.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:10:59 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:54 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

static void		parse_sect32(int *norme, SGC32 *seg, char *ar, int len)
{
	int			i;
	SEC32		*sect;
	void		*ptr;

	i = 0;
	ptr = (void *)(seg + 1);
	while (i < norme[0])
	{
		sect = (SEC32 *)ptr;
		if (norme[1])
			rev(sect->sectname, 16);
		if (!(ft_strcmp(sect->sectname, "__bss")))
			ar[len] = 'b';
		else if (!(ft_strcmp(sect->sectname, "__data")))
			ar[len] = 'd';
		else if (!(ft_strcmp(sect->sectname, "__text")))
			ar[len] = 't';
		else
			ar[len] = 's';
		i++;
		len++;
		ptr += sizeof(SEC32);
	}
}

void			get_sect32(LC *lc, char *arsect, size_t order)
{
	SGC32		*seg;
	int			len;
	int			norme[2];

	seg = (SGC32 *)lc;
	len = ft_strlen(arsect);
	norme[0] = seg->nsects;
	norme[1] = order;
	parse_sect32(norme, seg, arsect, len);
}

static void		parse_sect64(int *norme, SGC64 *seg, char *ar, int len)
{
	int			i;
	SEC64		*sect;
	void		*ptr;

	i = 0;
	ptr = (void *)(seg + 1);
	while (i < norme[0])
	{
		sect = (SEC64 *)ptr;
		if (norme[1])
			rev((char *)ptr, sizeof(SEC64));
		if (!(ft_strcmp(sect->sectname, "__bss")))
			ar[len] = 'b';
		else if (!(ft_strcmp(sect->sectname, "__data")))
			ar[len] = 'd';
		else if (!(ft_strcmp(sect->sectname, "__text")))
			ar[len] = 't';
		else
			ar[len] = 's';
		i++;
		len++;
		ptr += sizeof(SEC64);
	}
}

void			get_sect64(LC *lc, char *arsect, size_t order)
{
	SGC64		*seg;
	int			len;
	int			norme[2];

	seg = (SGC64 *)lc;
	len = ft_strlen(arsect);
	norme[0] = seg->nsects;
	norme[1] = order;
	parse_sect64(norme, seg, arsect, len);
}
