/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_errors.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 19:33:59 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:27 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

int			print_errors(char *ft, char *file, int code)
{
	ft_putstr_fd(ft, 2);
	if (file)
	{
		ft_putstr_fd(": ", 2);
		ft_putstr_fd(file, 2);
	}
	if (!code)
		ft_putstr_fd(": No such file or directory.\n", 2);
	if (code == 1)
		ft_putstr_fd(": Is a directory.\n", 2);
	if (code == 2)
		ft_putstr_fd(": The file is not a valid object file.\n", 2);
	if (code == 3)
		ft_putstr_fd(": Permission denied.\n", 2);
	if (code == 4)
		ft_putstr_fd(": At least one file must be specified.\n", 2);
	return (1);
}
