/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_fatbin.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:11:26 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:31 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

static int		count_elems(t_fs *lst)
{
	t_fs		*tmp;
	int			ret;

	ret = 0;
	tmp = lst;
	while (tmp)
	{
		ret++;
		tmp = tmp->next;
	}
	return (ret);
}

static void		print_arch(t_fs *lst, char *file)
{
	ft_putstr(file);
	if (lst->cput == -1)
		ft_putstr(" (for any architecture):\n");
	if (lst->cput == 1)
		ft_putstr(" (for architecture VAX:\n");
	if (lst->cput == 6)
		ft_putstr(" (for architecture MC680x0:\n");
	if (lst->cput == 7)
		ft_putstr(" (for architecture i386):\n");
	if (lst->cput == 8)
		ft_putstr(" (for architecture MIPS):\n");
	if (lst->cput == 10)
		ft_putstr(" (for architecture MC98000):\n");
	if (lst->cput == 11)
		ft_putstr(" (for architecture HPPA):\n");
	if (lst->cput == 12)
		ft_putstr(" (for architecture ARM):\n");
	if (lst->cput == 13)
		ft_putstr(" (for architecture MC88000):\n");
	if (lst->cput == 14)
		ft_putstr(" (for architecture SPARC):\n");
	if (lst->cput == 15)
		ft_putstr(" (for architecture i860):\n");
	if (lst->cput == 18)
		ft_putstr(" (for architecture PPC):\n");
}

static int		send_to_parsing(char *file, t_fs *lst, char *buf, size_t order)
{
	int			n;
	int			ret;

	n = count_elems(lst);
	ret = 0;
	while (lst)
	{
		if (n > 1)
			print_arch(lst, file);
		if (order)
			rev(buf + lst->offset, lst->size);
		ret = mnumber(file, buf + lst->offset, lst->size, "ft_nm");
		free(lst);
		lst = lst->next;
		ft_putchar('\n');
	}
	return (ret);
}

static int		get_lstfs(int max, FA *arch, char *buf, t_fs **lst)
{
	int			i;
	t_fs		*new;

	i = 0;
	while (i < max)
	{
		if (!(new = (t_fs *)malloc(sizeof(t_fs))))
			return (1);
		new->mnumber = *(int *)(buf + arch[i].offset);
		new->offset = arch[i].offset;
		new->size = arch[i].size;
		new->cput = arch[i].cputype;
		new->next = NULL;
		place_node_fs(lst, new);
		i++;
	}
	return (0);
}

int				handle_fatbin(char *file, char *buf, size_t size, int order)
{
	FH			*header;
	int			nstructs;
	FA			*arch;
	t_fs		*lst;

	lst = NULL;
	if (order)
		rev(buf, size);
	header = (FH *)buf;
	nstructs = header->nfat_arch;
	arch = (FA *)(buf + sizeof(FH *));
	if ((void *)arch >= (void *)buf + size)
		return (print_errors("ft_nm", NULL, 2));
	if (get_lstfs(nstructs, arch, buf, &lst))
		return (1);
	return (send_to_parsing(file, lst, buf, (order) ? size : 0));
}
