/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   handle_ranlib.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/18 19:34:41 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:34 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

static int		send_to_parsing(char *file, t_rl *lst, char *buf, size_t *norme)
{
	int			ret;
	char		*tmp;

	while (lst)
	{
		ft_putstr(file);
		ft_putchar('(');
		ft_putstr(buf + lst->offname);
		ft_putendl("):");
		tmp = buf + lst->offname + lst->lenname;
		ret = mnumber(file, tmp, norme[0], (norme[1]) ? "ft_nm" : "ft_otool");
		if (ret)
			break ;
		ft_putchar('\n');
		free(lst);
		lst = lst->next;
	}
	return (ret);
}

static int		get_lstrl(RL *ran, int n, char *buf, t_rl **lst)
{
	int			i;
	t_rl		*new;

	i = 0;
	while (i < n)
	{
		if (!(new = (t_rl *)malloc(sizeof(t_rl))))
			return (1);
		new->offset = ran[i].ran_off;
		new->offname = ran[i].ran_off + 60;
		new->lenname = ft_atoi(buf + ran[i].ran_off + 3);
		new->next = NULL;
		place_node_offset(lst, &new);
		i++;
	}
	return (0);
}

int				handle_ranlib(char *file, char *buf, size_t order, char *ft)
{
	int			nrans;
	char		*tmpbuf;
	t_rl		*lst;
	size_t		norme[2];

	lst = NULL;
	tmpbuf = &buf[68];
	if (*(int *)tmpbuf == 0x72613c21)
		return (0);
	if (*(int *)tmpbuf != 0x532e5f5f)
		return (mnumber(file, tmpbuf, order, ft));
	tmpbuf = &tmpbuf[20];
	nrans = *(int *)tmpbuf / sizeof(RL);
	if (get_lstrl((RL *)(tmpbuf + sizeof(int)), nrans, buf, &lst))
		return (1);
	remove_duplicates_offset(&lst);
	norme[0] = order;
	norme[1] = (!ft_strcmp(ft, "ft_nm")) ? 1 : 0;
	return (send_to_parsing(file, lst, buf, norme));
}
