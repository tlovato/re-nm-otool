/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_fs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/30 17:23:49 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:37 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

static int		check_arch(t_fs **lst, t_fs *new, t_fs *tmp)
{
	if ((new->mnumber != 0xfeedface && new->mnumber != 0xcefaedfe)
		&& (tmp->mnumber == 0xcefaedfe || tmp->mnumber == 0xfeedface))
	{
		*lst = new;
		return (1);
	}
	else if ((new->mnumber == 0xfeedface || new->mnumber == 0xcefaedfe)
		&& (tmp->mnumber != 0xcefaedfe && tmp->mnumber != 0xfeedface))
		return (1);
	return (0);
}

void			place_node_fs(t_fs **lst, t_fs *new)
{
	t_fs		*tmp;

	tmp = *lst;
	if (!(*lst))
		*lst = new;
	else
	{
		while (tmp->next)
		{
			if (check_arch(lst, new, tmp))
				return ;
			tmp = tmp->next;
		}
		if (check_arch(lst, new, tmp))
			return ;
		tmp->next = new;
	}
}
