/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_rl.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/30 19:01:22 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:39 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

void			place_node_offset(t_rl **start, t_rl **new)
{
	t_rl		*prev;
	t_rl		*begin;

	prev = NULL;
	begin = *start;
	while (*start && (*start)->offset < (*new)->offset)
	{
		prev = *start;
		*start = (*start)->next;
	}
	if (!prev)
	{
		(*new)->next = *start;
		*start = *new;
	}
	else
	{
		(*new)->next = prev->next;
		prev->next = *new;
		*start = begin;
	}
}

void			remove_duplicates_offset(t_rl **lst)
{
	t_rl		*tmp;
	t_rl		*save;

	tmp = *lst;
	while (tmp)
	{
		if (tmp->next && tmp->offset == tmp->next->offset)
		{
			save = tmp;
			while (save->next && save->offset == save->next->offset)
				save = save->next;
			tmp->next = save->next;
		}
		tmp = tmp->next;
	}
}
