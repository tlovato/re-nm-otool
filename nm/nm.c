/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   nm.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:12:36 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:44 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

int					main(int ac, char **av)
{
	int				i;
	int				ret;

	i = 1;
	if (ac == 1)
	{
		av[1] = ft_strdup("a.out");
		av[2] = NULL;
	}
	while (av[i])
	{
		if (ac > 2)
		{
			ft_putstr(av[i]);
			ft_putendl(":");
		}
		ret = map_file(av[i], "ft_nm");
		if (!ft_strcmp(av[1], "a.out"))
			free(av[1]);
		if (ac > 2)
			ft_putchar('\n');
		i++;
	}
	return (ret);
}
