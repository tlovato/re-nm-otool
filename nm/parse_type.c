/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_type.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:13:29 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:49 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

char				type(unsigned char t, unsigned char sect, char *ar, int val)
{
	unsigned char			ext;

	ext = t & N_EXT;
	if (!(t & N_STAB) && (t & N_TYPE) == N_UNDF)
	{
		if (ext && val)
			return ('C');
		return (ext ? 'U' : 'u');
	}
	if (!(t & N_STAB) && (t & N_TYPE) == N_ABS)
		return (ext ? 'A' : 'a');
	if (!(t & N_STAB) && (t & N_TYPE) == N_INDR)
		return (ext ? 'I' : 'i');
	if (!(t & N_STAB) && (t & N_TYPE) == N_SECT)
		return (ext ? ar[sect - 1] - 32 : ar[sect - 1]);
	return ('Z');
}
