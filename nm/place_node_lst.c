/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   place_node_lst.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:14:50 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:52 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

void			place_node_ascii(t_symb **lst, t_symb *new)
{
	t_symb	*tmp;

	tmp = *lst;
	if (!(*lst))
		*lst = new;
	else if (ft_strcmp(new->name, (*lst)->name) < 0)
	{
		new->next = *lst;
		*lst = new;
	}
	else
	{
		while (tmp->next)
		{
			if (ft_strcmp(new->name, tmp->next->name) < 0)
			{
				new->next = tmp->next;
				tmp->next = new;
				return ;
			}
			tmp = tmp->next;
		}
		tmp->next = new;
	}
}
