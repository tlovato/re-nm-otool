/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_infos.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:15:42 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:56:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

void				print_infos(int nbits, t_symb *lst)
{
	while (lst)
	{
		if (lst->type != 'Z')
		{
			if (lst->type != 'U')
			{
				if (nbits == 64)
					ft_printf("%.16lx ", lst->value);
				else if (nbits == 32)
					ft_printf("%.8lx ", lst->value);
			}
			else
			{
				if (nbits == 64)
					ft_printf("%16c ", ' ');
				else if (nbits == 32)
					ft_printf("%8c ", ' ');
			}
			ft_putchar(lst->type);
			ft_putchar(' ');
			ft_putendl(lst->name);
		}
		free(lst);
		lst = lst->next;
	}
}
