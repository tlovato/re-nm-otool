/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   remove_duplicates.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/17 15:43:48 by Pyxis             #+#    #+#             */
/*   Updated: 2018/06/30 19:56:57 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

void			remove_duplicates_ascii(t_symb **lst)
{
	t_symb		*tmp;
	t_symb		*save;
	t_symb		*prev;

	tmp = *lst;
	prev = NULL;
	while (tmp->next)
	{
		if (!ft_strcmp(tmp->name, tmp->next->name)
			&& tmp->type != tmp->next->type)
		{
			save = tmp->next;
			free(tmp);
			tmp = save;
			if (prev)
				prev->next = tmp;
			else
				*lst = tmp;
		}
		prev = tmp;
		tmp = tmp->next;
	}
}
