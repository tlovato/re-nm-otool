/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/20 17:44:56 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:00 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

char			*rev(char *buf, size_t size)
{
	size_t		i;
	int32_t		*tmp;

	i = 0;
	size = size / 4;
	tmp = (void *)buf;
	while (i < size)
	{
		tmp[i] = ((tmp[i] & 0xff000000) >> 24) |
				((tmp[i] & 0x00ff0000) >> 8) |
				((tmp[i] & 0x0000ff00) << 8) | (tmp[i] << 24);
		i++;
	}
	return (buf);
}

char			rev_byte(char val)
{
	val = (((val & 0xF0) >> 4) | ((val & 0x0F) << 4));
	val = (((val & 0xCC) >> 2) | ((val & 0x33) << 2));
	val = (((val & 0xAA) >> 1) | ((val & 0x55) << 1));
	return (val);
}
