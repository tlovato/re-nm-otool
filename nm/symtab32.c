/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symtab32.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:16:34 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:02 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

static void		init_t_symb32(t_symb *new, NL32 ar, t_var var, char *name)
{
	new->name = name;
	new->value = ar.n_value;
	new->type = type(ar.n_type, ar.n_sect, var.arsect, ar.n_value);
	new->next = NULL;
}

static int		get_lst32(NL32 *ar, t_var var, t_symb **l)
{
	int			i;
	char		*n;
	t_symb		*new;

	i = 0;
	while (i < var.max)
	{
		if (!(n = get_name(var.table + ar[i].n_un.n_strx)))
			return (1);
		if (!(new = (t_symb *)malloc(sizeof(t_symb))))
			return (1);
		init_t_symb32(new, ar[i], var, n);
		place_node_ascii(l, new);
		i++;
	}
	return (0);
}

static int		get_infos32(SC *sym, char *buf, char *arsect, size_t *order)
{
	NL32		*ar;
	char		*table;
	t_symb		*lst;
	t_var		var;

	lst = NULL;
	ar = (void *)buf + sym->symoff;
	if ((void *)ar >= (void *)buf + order[1])
		return (print_errors("ft_nm", NULL, 2));
	table = (void *)buf + sym->stroff;
	if (order[0])
		rev(table, sym->strsize);
	var.max = sym->nsyms;
	var.table = table;
	var.arsect = arsect;
	if (get_lst32(ar, var, &lst))
		return (1);
	remove_duplicates_ascii(&lst);
	print_infos(32, lst);
	return (0);
}

static void		norme_sucks(size_t *infos, int order, size_t size)
{
	infos[0] = order;
	infos[1] = size;
}

int				symtab32(char *buf, size_t size, int order)
{
	MH32		*header;
	int			i;
	char		arsect[255];
	size_t		infos[2];

	LC * lc;
	i = 0;
	ft_bzero(arsect, 255);
	if (order)
		rev(buf, size);
	header = (MH32 *)buf;
	lc = (void *)buf + sizeof(*header);
	norme_sucks(infos, order, size);
	while (i < header->ncmds)
	{
		if (lc->cmd == LC_SEGMENT)
			get_sect32(lc, arsect, order);
		if (lc->cmd == LC_SYMTAB)
			return (get_infos32((SC *)lc, buf, arsect, infos));
		i++;
		lc = (void *)lc + lc->cmdsize;
		if ((void *)lc >= (void *)buf + size)
			return (print_errors("ft_nm", NULL, 2));
	}
	return (1);
}
