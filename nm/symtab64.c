/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symtab64.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 17:21:13 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:05 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/nm.h"

static void		init_t_symb64(t_symb *new, NL64 ar, t_var var, char *name)
{
	new->name = name;
	new->value = ar.n_value;
	new->type = type(ar.n_type, ar.n_sect, var.arsect, ar.n_value);
	new->next = NULL;
}

static int		get_lst64(NL64 *ar, t_var var, t_symb **l)
{
	int			i;
	char		*n;
	t_symb		*new;

	i = 0;
	while (i < var.max)
	{
		if (!(n = get_name(var.table + ar[i].n_un.n_strx)))
			return (1);
		if (!(new = (t_symb *)malloc(sizeof(t_symb))))
			return (1);
		init_t_symb64(new, ar[i], var, n);
		place_node_ascii(l, new);
		i++;
	}
	return (0);
}

static int		get_infos64(SC *sym, char *buf, char *arsect, size_t *order)
{
	NL64		*ar;
	char		*table;
	t_symb		*lst;
	t_var		var;

	lst = NULL;
	ar = (void *)buf + sym->symoff;
	if ((void *)ar >= (void *)buf + order[1])
		return (print_errors("ft_nm", NULL, 2));
	table = (void *)buf + sym->stroff;
	if (order[0])
		rev(table, sym->strsize);
	var.max = sym->nsyms;
	var.table = table;
	var.arsect = arsect;
	if (get_lst64(ar, var, &lst))
		return (1);
	remove_duplicates_ascii(&lst);
	print_infos(64, lst);
	return (0);
}

static void		norme_sucks(size_t *infos, int order, size_t size)
{
	infos[0] = order;
	infos[1] = size;
}

int				symtab64(char *buf, size_t size, int order)
{
	MH64		*header;
	int			i;
	char		arsect[4096];
	size_t		infos[2];

	LC * lc;
	i = 0;
	ft_bzero(arsect, 4096);
	if (order)
		rev(buf, size);
	header = (MH64 *)buf;
	lc = (void *)buf + sizeof(*header);
	norme_sucks(infos, order, size);
	while (i < header->ncmds)
	{
		if (lc->cmd == LC_SEGMENT_64)
			get_sect64(lc, arsect, order);
		if (lc->cmd == LC_SYMTAB)
			return (get_infos64((SC *)lc, buf, arsect, infos));
		i++;
		lc = (void *)lc + lc->cmdsize;
		if ((void *)lc >= (void *)buf + size)
			return (print_errors("ft_nm", NULL, 2));
	}
	return (1);
}
