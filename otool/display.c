/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 14:38:31 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:08 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/otool.h"

void		display_result(int *norme, uint64_t a, char *buf, int n)
{
	int		off;
	int		i;

	off = 0;
	while (norme[0] + off < norme[1])
	{
		if (n == 32)
			ft_printf("%.8x\t", a + off);
		else if (n == 64)
			ft_printf("%.16llx\t", a + off);
		i = 0;
		while (i < 16 && norme[0] + off < norme[1])
		{
			ft_printf("%.2x ", *(unsigned char *)(buf + norme[0] + off));
			off++;
			i++;
		}
		ft_printf("\n");
	}
}
