/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_files.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 14:08:11 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:26 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/otool.h"

int					map_file(char *file, char *ft)
{
	char			*buf;
	struct stat		st;
	int				fd;

	if ((fd = open(file, O_RDONLY)) < 0)
		return (print_errors(ft, file, (errno == EACCES) ? 3 : 0));
	if (fstat(fd, &st) < 0)
		return (1);
	if (S_ISDIR(st.st_mode))
		return (print_errors(ft, file, 1));
	if ((buf = mmap(0, st.st_size, P_R, M_P, fd, 0)) == MAP_FAILED)
		return (print_errors(ft, file, 2));
	if (mnumber(file, buf, st.st_size, ft))
		return (1);
	if ((munmap(buf, st.st_size)) == -1)
		return (1);
	return (0);
}
