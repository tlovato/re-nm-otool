/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   otool.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 18:47:10 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:29 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/otool.h"

int			main(int ac, char **av)
{
	int		i;
	int		ret;

	i = 1;
	if (ac == 1)
		return (print_errors("ft_otool", NULL, 0));
	while (av[i])
	{
		ft_putstr(av[i]);
		ft_putendl(":");
		ret = map_file(av[i], "ft_otool");
		i++;
	}
	return (ret);
}
