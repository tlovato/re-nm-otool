/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_mnumber.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 14:09:42 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:32 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/otool.h"

int					mnumber(char *file, char *buf, int size, char *ft)
{
	int				mnumber;

	mnumber = *(int *)buf;
	if (!strcmp(ft, "ft_otool"))
		ft_putstr("Contents of (__TEXT,__text) section\n");
	if (mnumber == 0xfeedface)
		return (symtab32(buf, size, 0));
	else if (mnumber == 0xcefaedfe)
		return (symtab32(buf, size, 1));
	else if (mnumber == 0xfeedfacf)
		return (symtab64(buf, size, 0));
	else if (mnumber == 0xcffaedfe)
		return (symtab64(buf, size, 1));
	else if (mnumber == 0xcafebabe)
		return (handle_fatbin(file, buf, size, 0));
	else if (mnumber == 0xbebafeca)
		return (handle_fatbin(file, buf, size, 1));
	else if (mnumber == 0x72613c21)
		return (handle_ranlib(file, buf, size, ft));
	else
		return (print_errors(ft, file, 2));
	return (0);
}
