/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symtab32.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/19 19:30:11 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:38 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/otool.h"

static int		text_sect_32(int nsects, void *sectptr, void *buf)
{
	SEC32		*sect;
	int			i;
	uint32_t	end;
	int			norme[2];

	i = 0;
	while (i < nsects)
	{
		sect = (SEC32 *)sectptr;
		if (!ft_strcmp(sect->sectname, "__text"))
		{
			end = sect->offset + sect->size;
			norme[0] = sect->offset;
			norme[1] = end;
			display_result(norme, sect->addr, buf, 32);
			return (0);
		}
		sectptr += sizeof(SEC32);
		i++;
	}
	return (1);
}

int				symtab32(char *buf, size_t size, int order)
{
	MH32		*header;
	SGC32		*seg;
	int			i;

	i = 0;
	LC * lc;
	if (order)
		rev(buf, size);
	header = (MH32 *)buf;
	lc = (void *)buf + sizeof(*header);
	while (i < header->ncmds)
	{
		if (lc->cmd == LC_SEGMENT)
		{
			seg = (SGC32 *)lc;
			text_sect_32(seg->nsects, (void *)(seg + 1), buf);
		}
		lc = (void *)lc + lc->cmdsize;
		i++;
	}
	return (1);
}
