/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   symtab64.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/06/20 14:41:00 by tlovato           #+#    #+#             */
/*   Updated: 2018/06/30 19:57:41 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../incs/otool.h"

static int		text_sect_64(int nsects, void *sectptr, void *buf)
{
	SEC64		*sect;
	int			i;
	uint64_t	end;
	int			norme[2];

	i = 0;
	while (i < nsects)
	{
		sect = (SEC64 *)sectptr;
		if (!ft_strcmp(sect->sectname, "__text"))
		{
			end = sect->offset + sect->size;
			norme[0] = sect->offset;
			norme[1] = end;
			display_result(norme, sect->addr, buf, 64);
			return (0);
		}
		sectptr += sizeof(SEC64);
		i++;
	}
	return (1);
}

int				symtab64(char *buf, size_t size, int order)
{
	MH64		*header;
	SGC64		*seg;
	int			i;

	i = 0;
	LC * lc;
	if (order)
		rev(buf, size);
	header = (MH64 *)buf;
	lc = (void *)buf + sizeof(*header);
	while (i < header->ncmds)
	{
		if (lc->cmd == LC_SEGMENT_64)
		{
			seg = (SGC64 *)lc;
			text_sect_64(seg->nsects, (void *)(seg + 1), buf);
		}
		lc = (void *)lc + lc->cmdsize;
		i++;
	}
	return (1);
}
